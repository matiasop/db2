import sqlite3

con = sqlite3.connect("db1.db")
cursor = con.cursor()

def getAntallBilletterOnKundeordre():
    cursor.execute(''' 
    SELECT B.Ordrenummer, count(*) as Antall
    FROM Billett B
    WHERE B.Ordrenummer = ?
    GROUP BY B.Ordrenummer
    ''', (getMaxOrdrenummer(),))
    rows = cursor.fetchall()
    return rows

def updateKundeordreAntall():
    cursor.execute("UPDATE Kundeordre SET Antall = ? WHERE Ordrenummer = ?", (getAntallBilletterOnKundeordre()[0][1], getMaxOrdrenummer(),))
    con.commit()

def getallKupeNumbers(dato, ukedag, togruteID):
    cursor.execute(''' 
    SELECT S.Kupenummer
    FROM Billett B INNER JOIN Seng S ON B.Sengnummer = S.Sengnummer
    WHERE B.Dato = ?
    AND B.Ukedag = ?
    AND B.TogruteID = ?
    ''', (dato, ukedag, togruteID))
    rows = cursor.fetchall()
    return rows

def getKupeNummer(sengNummer):
    cursor.execute(''' 
    SELECT S.Kupenummer
    FROM Seng S
    WHERE S.Sengnummer = ?
    ''', (sengNummer,))
    row = cursor.fetchone()
    return row

def findMaxVognnummer(togruteID):
    cursor.execute(''' 
    SELECT MAX(TilhorendeVognoppsett.Vognnummer)
    FROM Togrute 
    NATURAL JOIN TilhorendeVognoppsett
    WHERE Togrute.TogruteID = ?
    ''', (togruteID,))
    row = cursor.fetchone()
    return row


def findVognoppsett(togruteID):
    cursor.execute(''' 
    SELECT V.Navn, TV.Vognnummer
    FROM Togrute T, TilhorendeVognoppsett TV, Vogntype V
    WHERE T.VognoppsettID = TV.VognoppsettID AND TV.VogntypeID = V.VogntypeID
    AND T.TogruteID = ?
    ''', (togruteID,))
    rows = cursor.fetchall()
    return rows

def findSitteVogntypeID(setenummer, togruteID, vognnummer):
    cursor.execute(''' 
    SELECT VogntypeID
    FROM Togrute NATURAL JOIN Vognoppsett
    NATURAL JOIN TilhorendeVognoppsett
    NATURAL JOIN Vogntype
    NATURAL JOIN Sittevogn
    NATURAL JOIN Sete
    WHERE Setenummer = ?
    AND TogruteID = ?
    AND Vognnummer = ?
    ''', (setenummer, togruteID, vognnummer))
    rows = cursor.fetchall()
    return rows

def findSoveVogntypeID(sengNr, togruteID, vognnummer):
    cursor.execute(''' 
    SELECT VogntypeID
    FROM Togrute NATURAL JOIN Vognoppsett
    NATURAL JOIN TilhorendeVognoppsett
    NATURAL JOIN Vogntype
    NATURAL JOIN Sovevogn
    NATURAL JOIN Seng
    WHERE Sengnummer = ?
    AND TogruteID = ?
    AND Vognnummer = ?
    ''', (sengNr, togruteID, vognnummer))
    rows = cursor.fetchall()
    return rows

def findSitteVognID(setenummer, togruteID, vognnummer):
    cursor.execute(''' 
    SELECT SittevognID
    FROM Togrute NATURAL JOIN Vognoppsett
    NATURAL JOIN TilhorendeVognoppsett
    NATURAL JOIN Vogntype
    NATURAL JOIN Sittevogn
    NATURAL JOIN Sete
    WHERE Setenummer = ?
    AND TogruteID = ?
    AND Vognnummer = ?
    ''', (setenummer, togruteID, vognnummer))
    rows = cursor.fetchall()
    return rows

def findSoveVognID(sengnummer, togruteID, vognnummer):
    cursor.execute('''
    SELECT SovevognID
    FROM Togrute NATURAL JOIN Vognoppsett
    NATURAL JOIN TilhorendeVognoppsett
    NATURAL JOIN Vogntype
    NATURAL JOIN Sovevogn
    NATURAL JOIN Seng
    WHERE Sengnummer = ?
    AND TogruteID = ?
    AND Vognnummer = ?           
    ''', (sengnummer, togruteID, vognnummer))
    rows = cursor.fetchall()
    return rows

def getKuneOrdre():
    cursor.execute(''' 
    SELECT Ordrenummer
    FROM Kundeordre
    ''')
    rows = cursor.fetchall()
    return rows

def getNewMaxOrdrenummer():
    storrelse = getKuneOrdre()
    if (storrelse == []):
        return 1
    else:
        cursor.execute(''' 
        SELECT MAX(Ordrenummer)
        FROM Kundeordre
        ''')
        new_ordrenummer = int(cursor.fetchone()[0])
        return new_ordrenummer + 1

def getBillettID():
    cursor.execute(''' 
    SELECT BillettID
    FROM Billett
    ''')
    rows = cursor.fetchall()
    return rows

def getNewMaxBillettID():
    storrelse = getBillettID()
    if (storrelse == []):
        return 1
    else:
        cursor.execute(''' 
        SELECT MAX(BillettID)
        FROM Billett
        ''')
        new_billettID = cursor.fetchone()[0]
        return new_billettID + 1


def getMaxOrdrenummer():
    cursor.execute(''' 
    SELECT MAX(Ordrenummer)
    FROM Kundeordre
    ''')
    new_ordrenummer = cursor.fetchone()[0]
    return new_ordrenummer

def createKundeOrdre(kundenummer, dato, klokkeslett):
    cursor.execute(''' 
    INSERT INTO Kundeordre VALUES (?, ?, ?, ?, "NULL")
    ''', (getNewMaxOrdrenummer(), kundenummer, dato, klokkeslett))
    con.commit()
    
def createBillettSitteplass(TogruteID, VognoppsettID, Vognnummer, VogntypeID, SittevognID, Setenummer, Dato, Ukedag, Startstasjon, Sluttstasjon):
    cursor.execute('''
    INSERT INTO Billett VALUES (?, ?, ?, ?, ?, ?, Null, Null, ?, ?, ?, ?, ?, ?)
    '''
    , (getNewMaxBillettID(), getMaxOrdrenummer(), TogruteID, VognoppsettID, Vognnummer, VogntypeID, SittevognID, Setenummer, Dato, Ukedag, Startstasjon, Sluttstasjon))
    con.commit()

def createBillettSengeplass(TogruteID, VognoppsettID, Vognnummer, VogntypeID, SovevognID, sengnummer, Dato, Ukedag, Startstasjon, Sluttstasjon):
    cursor.execute('''
    INSERT INTO Billett VALUES (?, ?, ?, ?, ?, ?, ?, ?, Null, Null, ?, ?, ?, ?)
    ''', (getNewMaxBillettID(), getMaxOrdrenummer(), TogruteID, VognoppsettID, Vognnummer, VogntypeID, SovevognID, sengnummer, Dato, Ukedag, Startstasjon, Sluttstasjon))
    con.commit()
    

def getKunde(telefonnummer):
    cursor.execute("SELECT * FROM Kunde WHERE Telefonnummer = ?", (telefonnummer,))
    rows = cursor.fetchall()
    return rows

def findKundeEkstra(kundeTLF, kundeEmail):
    cursor.execute(
        '''SELECT * FROM Kunde WHERE Telefonnummer = ? AND \"E-post\" = ?''', (kundeTLF, kundeEmail))
    rows = cursor.fetchall()
    return rows

def getOrderedTickets(dato, ukedag, togruteID):
    cursor.execute(
        '''SELECT * FROM Billett WHERE Dato = ? AND Ukedag = ? AND TogruteID = ?''', (dato, ukedag, togruteID))
    rows = cursor.fetchall()
    return rows

def getAllSeats(ukedag, valgtVognNummer, togruteID, vognnummer):
    cursor.execute(''' 
    SELECT S.Setenummer, S.Radnummer, TV.Vognnummer
    FROM Sete S INNER JOIN Sittevogn SV ON S.SittevognID = SV.SittevognID
    INNER JOIN Vogntype V ON V.SittevognID = SV.SittevognID
    INNER JOIN TilhorendeVognoppsett TV ON V.VogntypeID = TV.VogntypeID
    INNER JOIN Vognoppsett VO ON TV.VognoppsettID = VO.VognoppsettID
    INNER JOIN Togrute T ON T.VognoppsettID = VO.VognoppsettID
    INNER JOIN TogruteTabell TT ON TT.TogruteID = T.TogruteID
    WHERE TT.Ukedag = ?
    AND TV.VognoppsettID = ?
    AND T.TogruteID = ?
    AND TV.Vognnummer = ?
    ''', (ukedag, valgtVognNummer, togruteID, vognnummer))
    rows = cursor.fetchall()
    return rows

def getAllBeds(ukedag, valgtVognNummer, togruteID, vognnummer):
    cursor.execute(''' 
    SELECT S.Sengnummer, S.Kupenummer, TV.Vognnummer
    FROM Togrute AS T
    NATURAL JOIN TogruteTabell AS TT
    NATURAL JOIN TilhorendeVognoppsett AS TV
    NATURAL JOIN  Vogntype AS V
    NATURAL JOIN Sovevogn AS SV
    NATURAL JOIN Seng AS S
    WHERE TT.Ukedag = ?
    AND TV.VognoppsettID = ?
    AND T.TogruteID = ?
    AND TV.Vognnummer = ?
    ''', (ukedag, valgtVognNummer, togruteID, vognnummer))
    rows = cursor.fetchall()
    return rows
    
def getAllStations():
    cursor.execute("SELECT StasjonsNavn FROM Jernbanestasjon")
    rows = cursor.fetchall()
    return rows
    
def findCorrectRoutes(startStasjon, sluttStasjon, ukedag):
    cursor.execute(
    ''' 
    SELECT DISTINCT T.Startstasjon, T.Avgangsstid, T.Sluttstasjon, T.Ankomsstid, TT.Ukedag, T.TogruteID, T.VognoppsettID
    FROM Togrute T INNER JOIN GaarInnom G ON G.TogruteID = T.TogruteID INNER JOIN TogruteTabell TT ON TT.TogruteID = G.TogruteID
    WHERE (T.Startstasjon = ?) AND (G.StasjonsNavn = ? OR T.Sluttstasjon = ?) AND TT.Ukedag = ?

    UNION

    SELECT distinct g1.StasjonsNavn AS Startstasjon, g1.Avgangstid, g2.StasjonsNavn AS Sluttstasjon, g2.Ankomsttid, TT.Ukedag, g1.TogruteID, T.VognoppsettID
    FROM GaarInnom AS g1 INNER JOIN  GaarInnom AS g2 ON (g1.Avgangstid < g2.Ankomsttid) INNER JOIN TogruteTabell TT ON TT.TogruteID = g1.TogruteID INNER JOIN Togrute T ON T.TogruteID = g1.TogruteID
    WHERE g1.TogruteID = g2.TogruteID AND (g1.StasjonsNavn = ? AND g2.StasjonsNavn = ?) AND (TT.Ukedag = ?)

    UNION

    SELECT distinct G.StasjonsNavn AS Startstasjon, G.Avgangstid, T1.Sluttstasjon as Sluttstasjon, T1.Ankomsstid, TT.Ukedag, T1.TogruteID, T1.VognoppsettID
    FROM Togrute T1 INNER JOIN GaarInnom G ON T1.TogruteID = G.TogruteID INNER JOIN TogruteTabell TT ON TT.TogruteID = T1.TogruteID
    WHERE T1.Sluttstasjon = ? AND G.StasjonsNavn = ? AND TT.Ukedag = ? 
    '''
    ,(startStasjon, sluttStasjon, sluttStasjon, ukedag, startStasjon, sluttStasjon, ukedag, sluttStasjon, startStasjon, ukedag))
    
    rows = cursor.fetchall()    
    if (len(rows) == 0):
        return False
    else:
        return rows

def run():
    programOver = False
    while (programOver == False):
        user = None
        while True:
            telefonNummer = input("Skriv inn telefonnummeret ditt: ")
            users = getKunde(telefonNummer)
            if len(users) == 0:
                print("fIngen registrerte kunder med {telefonNummer} som telefonnummer") 
            elif len(users) > 1:
                print("Det er flere kunder registrert med dette telefonnummeret.")
                kundeEmail = input("Skriv inn e-post for å identifisere deg: ")
                if len(findKundeEkstra(telefonNummer, kundeEmail)) == 0:
                    print(f"Ingen registrerte kunder med {telefonNummer} som telefonnummer og {kundeEmail} som e-post.")
                else:
                    user = findKundeEkstra(telefonNummer, kundeEmail)
                    break
            else:
                user = getKunde(telefonNummer)
                break
            
        createKundeOrdre(user[0][0], input("Hva er datoen i dag? (YYYYMMDD) "), input("Hva er klokka nå? (HH:MM) "))
        billetter = True
        while billetter:
            dato = int(input("\nSkriv inn dato på reisen (yyyymmdd): "))
            ukedag = input("Skriv inn ukedagen for reisen: ")

            # Finner togrute basert på start og sluttstasjon
            alle_stasjoner = getAllStations()
            print("\nHer er alle stationene du kan velge mellom: ")
            for stasjon in alle_stasjoner:
                print(f"  * {stasjon[0]}")
            print()
            stasjoner = {}

            startStasjon = input("Skriv inn startstasjonen: ")
            sluttStasjon = input("Skriv inn sluttStasjonen: ")
            
                    
            togruter = findCorrectRoutes(startStasjon, sluttStasjon, ukedag)

            while (togruter is False):
                print("Det finnes ingen togruter mellom stasjonene du oppga, prøv igjen ")
                startStasjon = input("Skriv inn startstasjonen: ")
                sluttStasjon = input("Skriv inn sluttStasjonen: ")
                togruter = findCorrectRoutes(startStasjon, sluttStasjon, ukedag)
                
            counter = 0
            print(f"\nHer er de tilgjengelige rutene mellom {startStasjon} og {sluttStasjon} på {ukedag} ({dato})")
            for rute in togruter:
                print(f"Rutenummer: {counter}:   [{rute[0]} -> {rute[2]}: Avgang (kl: {rute[1]}), Ankomst: kl: ({rute[3]})")
                counter += 1

            RuteID = int(input("\nVelg rutenummeret til den togruten du ønsker å ta: "))
            while (RuteID >= counter or RuteID < 0):
                RuteID = int(input("Ruten du har valgt eksisterer ikke, vennligst velg en ny: "))
                
            togrute = togruter[RuteID]
            print(f"Du har valgt: [{togrute[0]} -> {togrute[2]}: Avgang (kl: {togrute[1]}), Ankomst: kl: ({togrute[3]})\n")

            togruteID = int(togrute[5])

            if (togruteID == 1 or togruteID == 2):
                stasjoner.update({"Trondheim": 1})
                stasjoner.update({"Steinkjer": 2})
                stasjoner.update({"Mosjøen": 3})
                stasjoner.update({"Mo i Rana": 4})
                stasjoner.update({"Fauske": 5})
                stasjoner.update({"Bodø": 6})
            if (togruteID == 3):
                stasjoner.update({"Mo i Rana": 1})
                stasjoner.update({"Mosjøen": 2})
                stasjoner.update({"Steinkjer": 3})
                stasjoner.update({"Trondheim": 4})
            
            
            
            vognoppsett = findVognoppsett(togruteID)
            print("Her er de tilgjengelige vognene for den valgte ruten: ")
            for vogn in vognoppsett:
                print(f"Vognnummer [{vogn[1]}]: {vogn[0]} ")
            print()
            
            while (True):
                valgtVognNummer = int(input("Skriv inn ditt ønskede vognnummer: "))
                if (valgtVognNummer < 1 or valgtVognNummer > int(findMaxVognnummer(togruteID)[0])):
                    print("Vognnummeret du har valgt eksisterer ikke, vennligst velg et nytt")
                else:
                    break    
                
            valgt_vogn = vognoppsett[valgtVognNummer-1]

            print(f"Du valgte {valgt_vogn[0]}en med vognnummer: {valgt_vogn[1]}\n")

            brukerStartIndex = stasjoner.get(startStasjon)
            brukerSluttIndex = stasjoner.get(sluttStasjon)
            kjopBillettStartIndex = -1
            kjopBillettSluttIndex = -1

            orderedTickets = getOrderedTickets(dato, ukedag, togruteID)
            vognnummer = valgt_vogn[1]
            # Hvis du valgt sittevogn
            if (valgt_vogn[0] == "Sittevogn"):
                allSeats = getAllSeats(ukedag, togrute[6], togruteID, vognnummer)
                print("Dette er alle setene på den valgte togruten: ")
                for sete in allSeats:
                    print(f"    * setenummer: {sete[0]}, radnummer: {sete[1]}")

                bool1 = True
                while (bool1):
                    while True:
                        seteNr = int(input("Velg ditt ønskede setenummer: "))
                        if(seteNr >= 1 and seteNr <= 12) and (vognnummer >= 1 and vognnummer <= 2):
                            break
                        else:
                            print("Sete eller vogn eksisterer ikke, prøv igjen")
                    teller = 0
                    for ticket in orderedTickets:
                        kjopBillettStartIndex = stasjoner.get(ticket[12])
                        kjopBillettSluttIndex = stasjoner.get(ticket[13])
                        
                        if (brukerStartIndex >= kjopBillettStartIndex and brukerStartIndex <= kjopBillettSluttIndex and seteNr == ticket[9] and vognnummer == ticket[4]) :
                            print("Dette setet er opptatt, velg et annet")
                            teller += 1

                        elif (brukerSluttIndex >= kjopBillettStartIndex and brukerSluttIndex <= kjopBillettSluttIndex and seteNr == ticket[9] and vognnummer == ticket[4]):
                            print("Dette setet er opptatt, velg et annet")
                            teller += 1           

                        elif (brukerStartIndex <= kjopBillettStartIndex and brukerSluttIndex >= kjopBillettSluttIndex and seteNr == ticket[9] and vognnummer == ticket[4]):
                            print("Dette setet er opptatt, velg et annet")
                            teller += 1
                    
                        elif (brukerStartIndex >= kjopBillettStartIndex and brukerSluttIndex <= kjopBillettSluttIndex and seteNr == ticket[9] and vognnummer == ticket[4]):
                            print("Dette setet er opptatt, velg et annet")
                            teller += 1
                    
                    if(teller==0):
                        print(f"Du har nå reservert [sete: {seteNr} i vogn: {vognnummer}]")
                        sitteVognID = findSitteVognID(seteNr, togrute[5], vognnummer)
                        vogntypeID = findSitteVogntypeID(seteNr, togrute[5], vognnummer)
                        createBillettSitteplass(togrute[5], togrute[6], vognnummer, vogntypeID[0][0], sitteVognID[0][0], seteNr, dato, ukedag, startStasjon, sluttStasjon)
                        while True:
                            flereBilletter = input("\nVil du kjøpe flere billetter? (ja/nei) ")
                            if flereBilletter == "ja":
                                bool1 = False
                                billetter = True
                                break
                            elif flereBilletter == "nei":
                                billetter = False
                                bool1 = False
                                break
                            else:
                                print("Ugyldig input, prøv igjen")
            
            else:
                allBeds = getAllBeds(ukedag, togrute[6], togruteID, vognnummer)
                print("Dette er alle sengene på den valgte togruten: ")
                for bed in allBeds:
                    print(f"    * sengnummer: {bed[0]}, kupenummer: {bed[1]}")
                
                bool2 = True
                sengNr = ""
                while bool2:
                    while True:
                        sengNr = int(input("velg sengnummer: "))
                        if(sengNr >= 1 and sengNr <= 8):
                            break
                        else:
                            print("Seng eller vogn eksisterer ikke, prøv igjen")

                    teller = 0
                    for ticket in orderedTickets:
   
                        all_kupe_numbers = getallKupeNumbers(dato, ukedag, togrute[5])
                    
                        userKupeNummer = getKupeNummer(sengNr)
                        

                        if (userKupeNummer in all_kupe_numbers):
                            print("Kupen er opptatt, velg en annen")
                            teller += 1
                            break
                                          
                        
                    if(teller==0):
                        print(f"Du har nå reservert [seng: {sengNr} og kupee: {getKupeNummer(sengNr)[0]} i vogn: {vognnummer}]")
                        sovevognID = findSoveVognID(sengNr, togrute[5], vognnummer)
                        vogntypeID = findSoveVogntypeID(sengNr, togrute[5], vognnummer)

                        createBillettSengeplass(togrute[5], togrute[6], vognnummer, vogntypeID[0][0], sovevognID[0][0], sengNr,  dato, ukedag, startStasjon, sluttStasjon)
                        while True:
                            flereBilletter = input("\nVil du kjøpe flere billetter? (ja/nei) ")
                            if flereBilletter == "ja":
                                bool2 = False
                                billetter = True
                                break
                            elif flereBilletter == "nei":
                                billetter = False
                                bool2 = False
                                break
                            else:
                                print("Ugyldig input, prøv igjen")

        programOver = True
    updateKundeordreAntall()

run()
cursor.close()