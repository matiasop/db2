import sqlite3

con = sqlite3.connect("db1.db")
cursor = con.cursor()

def findKunde(kundeTLF):
    cursor.execute("SELECT * FROM Kunde WHERE Telefonnummer = ?", (kundeTLF,))
    rows = cursor.fetchall()
    return rows


def findKundeEkstra(kundeTLF, kundeEmail):
    cursor.execute(
        '''SELECT * FROM Kunde WHERE Telefonnummer = ? AND \"E-post\" = ?''', (kundeTLF, kundeEmail))
    rows = cursor.fetchall()
    return rows


def findBillett(kundeTLF, kundeEmail, dato):
    if (kundeEmail == None):
        cursor.execute('''
        select B.TogruteID, B.VognoppsettID, B.Vognnummer, B.VogntypeID, B.SovevognID, B.SittevognID, B.Setenummer, B.Sengnummer, B.Dato, T.Startstasjon, T.Sluttstasjon, T.Avgangsstid, T.Ankomsstid
        from Billett B Inner join Kundeordre K ON K.Ordrenummer = B.Ordrenummer inner join Togrute T ON T.TogruteID = B.TogruteID
        inner join Kunde Ku On Ku.Kundenummer = K.Kundenummer
        Where  B.Dato >= ? AND Ku.Telefonnummer = ?
        ''', (dato, kundeTLF))
    else:
        cursor.execute('''
        select B.TogruteID, B.VognoppsettID, B.Vognnummer, B.VogntypeID, B.SovevognID, B.SittevognID, B.Setenummer, B.Sengnummer, B.Dato, T.Startstasjon, T.Sluttstasjon, T.Avgangsstid, T.Ankomsstid
        from Billett B Inner join Kundeordre K ON K.Ordrenummer = B.Ordrenummer inner join Togrute T ON T.TogruteID = B.TogruteID
        inner join Kunde Ku On Ku.Kundenummer = K.Kundenummer
        Where  B.Dato >= ? AND Ku.Telefonnummer = ? AND Ku."E-post" = ?
        ''', (dato, kundeTLF, kundeEmail))

    rows = cursor.fetchall()
    return rows


def run():
    # Finner om kunde er registrert i databasen
    while True:
        kundeTLF = input("\nSKRIV INN TELEFONNUMMER: ")
        if len(findKunde(kundeTLF)) == 0:
            print("\nINGEN REGISTRERTE KUNDER MED " +
                  kundeTLF + " SOM TELEFONNUMMER.")
        elif len(findKunde(kundeTLF)) > 1:
            print("\nDET ER FLERE KUNDER REGISTRERT MED DETTE TELEFONNUMMERET.")
            kundeEmail = input("\nSKRIV INN E-POST: ")
            if len(findKundeEkstra(kundeTLF, kundeEmail)) == 0:
                print("\nINGEN REGISTRERTE KUNDER MED " + kundeTLF +
                      " SOM TELEFONNUMMER OG " + kundeEmail + " SOM E-POST.")
            else:
                break
        else:
            break

    # Skrive ut billetter registrert på kunden
    dato = int(input("\nSKRIV INN DATOEN I DAG PÅ FORMEN 'YYYYMMDD':"))
    if len(findKunde(kundeTLF)) > 1:
        if len(findBillett(kundeTLF, kundeEmail, dato)) == 0:
            print("Du har ingen reiser etter i dag.")
        else:
            print("Du har disse reisene etter i dag: ")
            for billett in findBillett(kundeTLF, kundeEmail, dato):
                print(billett)

    else:
        if len(findBillett(kundeTLF, None, dato)) == 0:
            print("Du har ingen reiser etter i dag.")
        else:
            print("Du har disse reisene etter i dag: ")
            for billett in findBillett(kundeTLF, None, dato):
                print(billett)


run()
