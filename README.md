# DB2

Gruppe 1
- Matias Opsahl
- Sander Skogh Linnerud
- Robin Tveråen


Python bibliotek som brukes
- sqlite3
- datetime

## Brukerhistorier
**Brukerhistorie C:**  For en stasjon som oppgis, skal bruker få ut alle togruter som er innom stasjonen en gitt ukedag.

**Brukerhistorie D:** Bruker skal kunne søke etter togruter som går mellom en startstasjon og en sluttstasjon, med
utgangspunkt i en dato og et klokkeslett. Alle ruter den samme dagen og den neste skal
returneres, sortert på tid.

**Brukerhistorie E:** En bruker skal kunne registrere seg i kunderegisteret.

**Brukerhistorie G:** Registrerte kunder skal kunne finne ledige billetter for en oppgitt strekning på en ønsket togrute
og kjøpe de billettene hen ønsker. 

**Brukerhistorie H:** For en bruker skal man kunne finne all informasjon om de kjøpene hen har gjort for fremtidige
reiser. 








## Intruksjoner
1. Kjør db2.py
2. Registrer bruker ved hjelp av brukerhistorieE.py

 

