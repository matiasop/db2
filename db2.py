import sqlite3
con = sqlite3.connect("db1.db")
cursor = con.cursor()


def createBanestrekning():
    cursor.execute(
        '''INSERT INTO Banestrekning VALUES (1, 'Nordlandsbanen', 'Diesel', 5)''')
    con.commit()


def getAllBanestrekning():
    cursor.execute("SELECT * FROM Banestrekning")
    rows = cursor.fetchall()
    print("Banestrekning:")
    print(rows)


def createJernbanestasjon():
    cursor.execute('''INSERT INTO Jernbanestasjon VALUES ('Trondheim', 5.1)''')
    cursor.execute('''INSERT INTO Jernbanestasjon VALUES ('Steinkjer', 3.6)''')
    cursor.execute('''INSERT INTO Jernbanestasjon VALUES ('Mosjøen', 6.8)''')
    cursor.execute('''INSERT INTO Jernbanestasjon VALUES ('Mo i Rana', 3.5)''')
    cursor.execute('''INSERT INTO Jernbanestasjon VALUES ('Fauske', 34.0)''')
    cursor.execute('''INSERT INTO Jernbanestasjon VALUES ('Bodø', 4.1)''')
    con.commit()


def getAllJernbanestasjon():
    cursor.execute("SELECT * FROM Jernbanestasjon")
    rows = cursor.fetchall()
    print("Jernbanestasjon:")
    print(rows)


def createDelstrekning():
    cursor.execute(
        '''INSERT INTO Delstrekning VALUES ('Trondheim', 'Steinkjer', 120, 'Dobbel')''')
    cursor.execute(
        '''INSERT INTO Delstrekning VALUES ('Steinkjer', 'Mosjøen', 280, 'Enkel')''')
    cursor.execute(
        '''INSERT INTO Delstrekning VALUES ('Mosjøen', 'Mo i Rana', 90, 'Enkel')''')
    cursor.execute(
        '''INSERT INTO Delstrekning VALUES ('Mo i Rana', 'Fauske', 170, 'Enkel')''')
    cursor.execute(
        '''INSERT INTO Delstrekning VALUES ('Fauske', 'Bodø', 60, 'Enkel')''')
    con.commit()


def getAllDelstrekning():
    cursor.execute("SELECT * FROM Delstrekning")
    rows = cursor.fetchall()
    print("Delstrekning:")
    print(rows)


def createSittevogn():
    cursor.execute('''INSERT INTO Sittevogn VALUES (1, 3)''')
    cursor.execute('''INSERT INTO Sittevogn VALUES (2, 3)''')
    con.commit()


def getAllSittevogn():
    cursor.execute("SELECT * FROM Sittevogn")
    rows = cursor.fetchall()
    print("Sittevogn:")
    print(rows)


def createSovevogn():
    cursor.execute('''INSERT INTO Sovevogn VALUES (1, 4)''')
    con.commit()


def getAllSovevogn():
    cursor.execute("SELECT * FROM Sovevogn")
    rows = cursor.fetchall()
    print("Sovevogn:")
    print(rows)


def createVogntype():
    cursor.execute('''INSERT INTO Vogntype VALUES (1, 'Sovevogn', null, 1)''')
    cursor.execute('''INSERT INTO Vogntype VALUES (2, 'Sittevogn', 1, null)''')
    cursor.execute('''INSERT INTO Vogntype VALUES (3, 'Sittevogn', 2, null)''')
    con.commit()


def getAllVogntype():
    cursor.execute("SELECT * FROM Vogntype")
    rows = cursor.fetchall()
    print("Vogntype:")
    print(rows)


def createOperator():
    cursor.execute('''INSERT INTO Operator VALUES (1, 'SJ', 1, 1)''')
    con.commit()


def getAllOperator():
    cursor.execute("SELECT * FROM Operator")
    rows = cursor.fetchall()
    print("Operator:")
    print(rows)


def createVognoppsett():
    cursor.execute('''INSERT INTO Vognoppsett VALUES (1, 2)''')
    cursor.execute('''INSERT INTO Vognoppsett VALUES (2, 2)''')
    cursor.execute('''INSERT INTO Vognoppsett VALUES (3, 1)''')
    con.commit()


def getAllVognoppsett():
    cursor.execute("SELECT * FROM Vognoppsett")
    rows = cursor.fetchall()
    print("Vognoppsett:")
    print(rows)

# Må legge til for alle dager (ikke 3, kun alle hverdager), kun mandag som ligger inne nå. Må oppdatere GaarInnom også


def createTogrute():
    cursor.execute(
        '''INSERT INTO Togrute VALUES (1, '09:05', '23:05', 2, 1, 'Trondheim', 'Bodø', 'Hovedretning', 1)''')
    cursor.execute(
        '''INSERT INTO Togrute VALUES (2, '17:34', '07:49', 1, 1, 'Trondheim', 'Bodø', 'Hovedretning', 1)''')
    cursor.execute(
        '''INSERT INTO Togrute VALUES (3, '14:13', '08:11', 3, 1, 'Mo i Rana', 'Trondheim', 'Mot hovedretning', 1)''')
    con.commit()


def getAllTogrute():
    cursor.execute("SELECT * FROM Togrute")
    rows = cursor.fetchall()
    print("Togrute:")
    print(rows)

def createTogruteTabell():
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Mandag', 1)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Tirsdag', 1)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Onsdag', 1)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Torsdag', 1)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Fredag', 1)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Lørdag', 1)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Søndag', 1)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Mandag', 2)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Tirsdag', 2)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Onsdag', 2)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Torsdag', 2)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Fredag', 2)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Mandag', 3)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Tirsdag', 3)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Onsdag', 3)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Torsdag', 3)''')
    cursor.execute('''INSERT INTO TogruteTabell VALUES ('Fredag', 3)''')
    con.commit()

def getAllTogruteTabell():
    cursor.execute("SELECT * FROM TogruteTabell")
    rows = cursor.fetchall()
    print("TogruteTabell:")
    print(rows)

def createGaarInnom():
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Steinkjer', 1, '00:57', '00:55')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Mosjøen', 1, '04:41', '04:39')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Mo i Rana', 1, '05:55', '05:53')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Fauske', 1, '08:19', '08:17')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Steinkjer', 2, '09:51', '09:49')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Mosjøen', 2, '13:20', '13:18')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Mo i Rana', 2, '14:31', '14:39')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Fauske', 2, '16:49', '16:47')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Mosjøen', 3, '09:14', '09:12')''')
    cursor.execute(
        '''INSERT INTO GaarInnom VALUES ('Steinkjer', 3, '12:31', '12:29')''')
    con.commit()


def getAllGaarInnom():
    cursor.execute("SELECT * FROM GaarInnom")
    rows = cursor.fetchall()
    print("Mellomstasjoner:")
    print(rows)


def createTilhorendeVognoppsett():
    cursor.execute('''INSERT INTO TilhorendeVognoppsett VALUES (1, 2, 1)''')
    cursor.execute('''INSERT INTO TilhorendeVognoppsett VALUES (1, 3, 2)''')
    cursor.execute('''INSERT INTO TilhorendeVognoppsett VALUES (2, 1, 1)''')
    cursor.execute('''INSERT INTO TilhorendeVognoppsett VALUES (2, 2, 2)''')
    cursor.execute('''INSERT INTO TilhorendeVognoppsett VALUES (3, 2, 1)''')
    con.commit()


def getAllTilhorendeVognoppsett():
    cursor.execute("SELECT * FROM TilhorendeVognoppsett")
    rows = cursor.fetchall()
    print("Vognoppsett til vogntype:")
    print(rows)


def createTilhorendeOperator():
    cursor.execute('''INSERT INTO TilhorendeOperator VALUES (1, 1)''')
    cursor.execute('''INSERT INTO TilhorendeOperator VALUES (1, 2)''')
    cursor.execute('''INSERT INTO TilhorendeOperator VALUES (1, 3)''')
    con.commit()

def getAllTilhorendeOperator():
    cursor.execute("SELECT * FROM TilhorendeOperator")
    rows = cursor.fetchall()
    print("Vogntype til operatør:")
    print(rows)

def createSeter():
    cursor.execute('''INSERT INTO Sete VALUES (1, 1, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (2, 1, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (3, 1, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (4, 1, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (5, 1, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (6, 1, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (7, 1, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (8, 1, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (9, 1, 3)''')
    cursor.execute('''INSERT INTO Sete VALUES (10, 1, 3)''')
    cursor.execute('''INSERT INTO Sete VALUES (11, 1, 3)''')
    cursor.execute('''INSERT INTO Sete VALUES (12, 1, 3)''')
    cursor.execute('''INSERT INTO Sete VALUES (1, 2, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (2, 2, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (3, 2, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (4, 2, 1)''')
    cursor.execute('''INSERT INTO Sete VALUES (5, 2, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (6, 2, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (7, 2, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (8, 2, 2)''')
    cursor.execute('''INSERT INTO Sete VALUES (9, 2, 3)''')
    cursor.execute('''INSERT INTO Sete VALUES (10, 2, 3)''')
    cursor.execute('''INSERT INTO Sete VALUES (11, 2, 3)''')
    cursor.execute('''INSERT INTO Sete VALUES (12, 2, 3)''')
    con.commit()

def createSenger():
    cursor.execute('''INSERT INTO Seng VALUES (1, 1, 1)''')
    cursor.execute('''INSERT INTO Seng VALUES (2, 1, 1)''')
    cursor.execute('''INSERT INTO Seng VALUES (3, 1, 2)''')
    cursor.execute('''INSERT INTO Seng VALUES (4, 1, 2)''')
    cursor.execute('''INSERT INTO Seng VALUES (5, 1, 3)''')
    cursor.execute('''INSERT INTO Seng VALUES (6, 1, 3)''')
    cursor.execute('''INSERT INTO Seng VALUES (7, 1, 4)''')
    cursor.execute('''INSERT INTO Seng VALUES (8, 1, 4)''')
    con.commit()


def createAll():
    createJernbanestasjon()
    createBanestrekning()
    createDelstrekning()
    createSittevogn()
    createSovevogn()
    createVogntype()
    createOperator()
    createVognoppsett()
    createTogrute()
    createTogruteTabell()
    createGaarInnom()
    createTilhorendeVognoppsett()
    createTilhorendeOperator()
    createSeter()
    createSenger()


def getAll():
    getAllJernbanestasjon()
    getAllBanestrekning()
    getAllDelstrekning()
    getAllSittevogn()
    getAllSovevogn()
    getAllVogntype()
    getAllOperator()
    getAllVognoppsett()
    getAllTogrute()
    getAllTogruteTabell()
    getAllGaarInnom()
    getAllTilhorendeVognoppsett()
    getAllTilhorendeOperator()

def deleteAll():
    cursor.execute('''DELETE FROM Banestrekning''')
    cursor.execute('''DELETE FROM Billett''')
    cursor.execute('''DELETE FROM Delstrekning''')
    cursor.execute('''DELETE FROM GaarInnom''')
    cursor.execute('''DELETE FROM Jernbanestasjon''')
    cursor.execute('''DELETE FROM Kunde''')
    cursor.execute('''DELETE FROM Kundeordre''')
    cursor.execute('''DELETE FROM Operator''')
    cursor.execute('''DELETE FROM Seng''')
    cursor.execute('''DELETE FROM Sete''')
    cursor.execute('''DELETE FROM Sittevogn''')
    cursor.execute('''DELETE FROM Sovevogn''')
    cursor.execute('''DELETE FROM TilhorendeOperator''')
    cursor.execute('''DELETE FROM TilhorendeVognoppsett''')
    cursor.execute('''DELETE FROM Togrute''')
    cursor.execute('''DELETE FROM TogruteTabell''')
    cursor.execute('''DELETE FROM Vognoppsett''')
    cursor.execute('''DELETE FROM Vogntype''')
    cursor.execute('''DELETE FROM Sete''')
    cursor.execute('''DELETE FROM Seng''')
    con.commit()


# Denne sletter all data:
deleteAll()   

# Denne lager all data:
createAll()

# Denne skriver ut all data:
# getAll()


con.close()
