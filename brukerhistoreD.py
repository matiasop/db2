import sqlite3
from datetime import datetime, timedelta

con = sqlite3.connect('db1.db')
cursor = con.cursor()

def ruter(startstasjon, sluttstasjon, ukedag, klokkeslett, nesteUkedag):
    cursor.execute('''
        SELECT DISTINCT T.Startstasjon, T.Avgangsstid, T.Sluttstasjon, T.Ankomsstid, TT.Ukedag, T.TogruteID, T.VognoppsettID
        FROM Togrute T INNER JOIN GaarInnom G ON G.TogruteID = T.TogruteID INNER JOIN TogruteTabell TT ON TT.TogruteID = G.TogruteID
        WHERE (T.Startstasjon = ?) AND (G.StasjonsNavn = ? OR T.Sluttstasjon = ?) AND TT.Ukedag = ? AND T.Avgangsstid > ?

        UNION

        SELECT distinct g1.StasjonsNavn AS Startstasjon, g1.Avgangstid, g2.StasjonsNavn AS Sluttstasjon, g2.Ankomsttid, TT.Ukedag, g1.TogruteID, T.VognoppsettID
        FROM GaarInnom AS g1 INNER JOIN  GaarInnom AS g2 ON (g1.Avgangstid < g2.Ankomsttid) INNER JOIN TogruteTabell TT ON TT.TogruteID = g1.TogruteID INNER JOIN Togrute T ON T.TogruteID = g1.TogruteID
        WHERE g1.TogruteID = g2.TogruteID AND (g1.StasjonsNavn = ? AND g2.StasjonsNavn = ?) AND (TT.Ukedag = ?) AND g1.Avgangstid > ?

        UNION

        SELECT distinct G.StasjonsNavn AS Startstasjon, G.Avgangstid, T1.Sluttstasjon as Sluttstasjon, T1.Ankomsstid, TT.Ukedag, T1.TogruteID, T1.VognoppsettID
        FROM Togrute T1 INNER JOIN GaarInnom G ON T1.TogruteID = G.TogruteID INNER JOIN TogruteTabell TT ON TT.TogruteID = T1.TogruteID
        WHERE T1.Sluttstasjon = ? AND G.StasjonsNavn = ? AND TT.Ukedag = ?  AND G.Avgangstid > ?

        UNION

        SELECT DISTINCT T.Startstasjon, T.Avgangsstid, T.Sluttstasjon, T.Ankomsstid, TT.Ukedag, T.TogruteID, T.VognoppsettID
        FROM Togrute T INNER JOIN GaarInnom G ON G.TogruteID = T.TogruteID INNER JOIN TogruteTabell TT ON TT.TogruteID = G.TogruteID
        WHERE (T.Startstasjon = ?) AND (G.StasjonsNavn = ? OR T.Sluttstasjon = ?) AND TT.Ukedag = ?

        UNION

        SELECT distinct g1.StasjonsNavn AS Startstasjon, g1.Avgangstid, g2.StasjonsNavn AS Sluttstasjon, g2.Ankomsttid, TT.Ukedag, g1.TogruteID, T.VognoppsettID
        FROM GaarInnom AS g1 INNER JOIN  GaarInnom AS g2 ON (g1.Avgangstid < g2.Ankomsttid) INNER JOIN TogruteTabell TT ON TT.TogruteID = g1.TogruteID INNER JOIN Togrute T ON T.TogruteID = g1.TogruteID
        WHERE g1.TogruteID = g2.TogruteID AND (g1.StasjonsNavn = ? AND g2.StasjonsNavn = ?) AND (TT.Ukedag = ?)

        UNION

        SELECT distinct G.StasjonsNavn AS Startstasjon, G.Avgangstid, T1.Sluttstasjon as Sluttstasjon, T1.Ankomsstid, TT.Ukedag, T1.TogruteID, T1.VognoppsettID
        FROM Togrute T1 INNER JOIN GaarInnom G ON T1.TogruteID = G.TogruteID INNER JOIN TogruteTabell TT ON TT.TogruteID = T1.TogruteID
        WHERE T1.Sluttstasjon = ? AND G.StasjonsNavn = ? AND TT.Ukedag = ?
    ''', (startstasjon, sluttstasjon, sluttstasjon, ukedag, klokkeslett, startstasjon, sluttstasjon, ukedag, klokkeslett, sluttstasjon, startstasjon, ukedag, klokkeslett, startstasjon, sluttstasjon, sluttstasjon, nesteUkedag, startstasjon, sluttstasjon, nesteUkedag, sluttstasjon, startstasjon, nesteUkedag,))
    rows = cursor.fetchall()
    return rows

def sort_key(record):
    day_map = {'Mandag': 1, 'Tirsdag': 2, 'Onsdag': 3, 'Torsdag': 4, 'Fredag': 5, 'Lørdag': 6, 'Søndag': 7}
    return (day_map[record[4]], record[1])

def run():
    startstasjon = input("Startstasjon: ")
    sluttstasjon = input("Sluttstasjon: ")
    dato = input("Dato (YYYY-MM-DD): ")
    klokkeslett = input("Klokkeslett (HH:MM): ")
    weekday = datetime.strptime(dato, "%Y-%m-%d").strftime("%A")
    ukedag = {
    "Monday": "Mandag",
    "Tuesday": "Tirsdag",
    "Wednesday": "Onsdag",
    "Thursday": "Torsdag",
    "Friday": "Fredag",
    "Saturday": "Lørdag",
    "Sunday": "Søndag"
    }[weekday]
    next_weekday = (datetime.strptime(dato, "%Y-%m-%d") + timedelta(days=1)).strftime("%A")
    nesteUkedag = {
        "Monday": "Mandag",
        "Tuesday": "Tirsdag",
        "Wednesday": "Onsdag",
        "Thursday": "Torsdag",
        "Friday": "Fredag",
        "Saturday": "Lørdag",
        "Sunday": "Søndag"
    }[next_weekday]
    muligeRuter = ruter(startstasjon, sluttstasjon, ukedag, klokkeslett, nesteUkedag)
    sorted_data = sorted(muligeRuter, key=sort_key)
    for rute in sorted_data:
        print("\nRUTE: ")
        print("Startstasjon: " + rute[0])
        print("Avgangstid: " + rute[1])
        print("Sluttstasjon: " + rute[2])
        print("Ankomsttid: " + rute[3])
        print("Ukedag: " + rute[4])
        print("Rutenummer: " + str(rute[5]))


run()
con.close()
