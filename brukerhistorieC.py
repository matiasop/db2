import sqlite3

con = sqlite3.connect("db1.db")
cursor = con.cursor()


def printStasjoner():
    cursor.execute("SELECT StasjonsNavn FROM Jernbanestasjon")
    stasjoner = cursor.fetchall()

    print("STASJONER:")
    for stasjon in stasjoner:
        print(stasjon[0])


def printRuter(brukerStasjon, brukerDag):
    query = '''
    SELECT T.Ankomsstid, T.Avgangsstid, TB.Ukedag, T.Startstasjon, T.Sluttstasjon
    FROM GaarInnom G, Togrute T
    INNER JOIN Jernbanestasjon J ON G.Stasjonsnavn = J.Stasjonsnavn
    INNER JOIN TogruteTabell TB ON TB.TogruteID = G.TogruteID
    WHERE J.Stasjonsnavn = ? AND T.TogruteID = G.TogruteID AND TB.Ukedag = ?

    UNION

    SELECT T.Ankomsstid, T.Avgangsstid, TB.Ukedag, T.Startstasjon, T.Sluttstasjon
    FROM Togrute T
    INNER JOIN Jernbanestasjon J ON T.Startstasjon = J.StasjonsNavn OR T.Sluttstasjon = J.StasjonsNavn
    INNER JOIN TogruteTabell TB ON TB.TogruteID = T.TogruteID
    WHERE J.StasjonsNavn = ? AND TB.Ukedag = ?
    '''
    cursor.execute(query, (brukerStasjon, brukerDag.capitalize(),
                   brukerStasjon, brukerDag.capitalize()))
    rows = cursor.fetchall()
    return rows


def checkStasjon(brukerStasjon):
    query = "SELECT Stasjonsnavn FROM Jernbanestasjon WHERE Stasjonsnavn = ?"
    cursor.execute(query, (brukerStasjon,))
    stasjon = cursor.fetchall()
    if (len(stasjon) != 0):
        return True
    else:
        return False


def checkRuter(brukerStasjon, brukerDag):
    query = '''
    SELECT G.TogruteID, TB.Ukedag
    FROM GaarInnom G, Togrute T
    INNER JOIN Jernbanestasjon J ON G.Stasjonsnavn = J.Stasjonsnavn
    INNER JOIN TogruteTabell TB ON TB.TogruteID = G.TogruteID
    WHERE J.Stasjonsnavn = ? AND T.TogruteID = G.TogruteID AND TB.Ukedag = ?

    UNION

    SELECT T.TogruteID, TB.Ukedag
    FROM Togrute T
    INNER JOIN Jernbanestasjon J ON T.Startstasjon = J.StasjonsNavn OR T.Sluttstasjon = J.StasjonsNavn
    INNER JOIN TogruteTabell TB ON TB.TogruteID = T.TogruteID
    WHERE J.StasjonsNavn = ? AND TB.Ukedag = ?
    '''
    cursor.execute(query, (brukerStasjon, brukerDag.capitalize(),
                   brukerStasjon, brukerDag.capitalize()))
    rows = cursor.fetchall()
    return rows


def run():
    while True:
        printStasjoner()
        brukerStasjon = input("\nSKRIV INN NAVNET PÅ EN STASJON: ")
        if checkStasjon(brukerStasjon):
            break
        print("\n" + brukerStasjon.capitalize() +
              " eksisterer ikke som stasjon.\n")
    while True:
        brukerDag = input("\nSKRIV INN EN UKEDAG: ")
        if len(checkRuter(brukerStasjon, brukerDag)) == 0:
            print("\n" + "Ingen togruter går innom " +
                  brukerStasjon + " på en " + brukerDag + ".")
        else:
            print("\nDette er togrutene som går innom " +
                  brukerStasjon + " på en " + brukerDag + ".")
            for rute in printRuter(brukerStasjon, brukerDag):
                print(rute)
            break


run()

con.close()
