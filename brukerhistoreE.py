import sqlite3

con = sqlite3.connect("db1.db")
cursor = con.cursor()

# Henter ut det høyeste kundenummeret (slik at vi kan lage ett nytt og unikt kundenummer)
cursor.execute("SELECT MAX(kundenummer) FROM Kunde")

# Prøver å hente ut verdien fra tuplen som returneres
max_kundenummer = cursor.fetchone()[0]

# Sjekker om det fantes data i tabellen fra før eller ikke
if max_kundenummer is None:
    next_kundenummer = 1
else:
    next_kundenummer = max_kundenummer + 1

# Henter input fra brukeren
navn = input("Skriv inn navnet ditt: ")
epost = input("Skriv inn din epost: ")
telefonnummer = input("Skriv inn ditt telefonnummer: ")

# Lager queryen og verdiene
query = '''INSERT INTO Kunde (Kundenummer, Navn, \"E-post\", Telefonnummer) VALUES (?, ?, ?, ?)'''
values = (next_kundenummer, navn, epost, telefonnummer)

print(f'''\nDu har nå registrert med følgende data:
    * Navn: {navn}
    * E-post: {epost}
    * Telefonnummer: {telefonnummer}\n''')

# Utfører queryen med de parametariserte verdiene
cursor.execute(query, values)
con.commit()

con.close()
